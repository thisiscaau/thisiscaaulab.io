---
title: Code chay
published: true
date: 2020-01-01 15:43:23
tags: random, exp, shitposting
description: Code không màu
image:
---
Cũng đã ngót ngét 1 tháng kể từ khi mình quyết tâm chuyển sang mode "code chay" này,
từ bỏ cuộc sống template xa hoa phù phiếm.

Đó là chuyển sang code bằng file txt =)) không syntax highlighting, không autocomplete,
không gì cả... đưa màn hình code về dạng đen trắng nguyên thủy.

![](img/chay.png)

Dont judge me but ... this does work.

Mọi việc bắt đầu từ khi mình code thi LAH offline bug vô học đến nỗi mà khác biệt
giữa full điểm và 0 điểm chỉ là 1-2 dòng code. Vì lý do đó mà mình quyết tâm "cải
cách" lại bản thân để bớt choke hơn.

Nhưng công nhận quá trình "khổ dâm" này đã đem lại kết quả tích cực không ngờ:
- Code đỡ bug hơn
- Code tập trung hơn
- Gõ nhanh hơn ? 
- Code sạch hơn
- Code nhìn ngầu hơn =))

Hãy thử dành ra một tuần để dùng thử và đưa ra cảm nhận của riêng mình nhé.
